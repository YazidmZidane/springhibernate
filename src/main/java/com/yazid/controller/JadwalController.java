package com.yazid.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.yazid.model.Jadwal;
import com.yazid.model.Ruangan;
import com.yazid.service.JadwalService;
import com.yazid.service.RuanganService;

@Controller
public class JadwalController {
	
	@Autowired
	JadwalService jadwalService;
	
	@Autowired
	RuanganService ruanganService;
	
	@RequestMapping(value = "/getAllJadwal", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getJadwal(Model model) {
		List listOfJadwal = jadwalService.getAllJadwal();
		List listOfRuangan = ruanganService.getAllRuangan();
		model.addAttribute("jadwal", new Jadwal());
		model.addAttribute("listOfJadwal", listOfJadwal);
		model.addAttribute("listOfRuangan", listOfRuangan);
		return "detailJadwal";
	}
	
	@RequestMapping(value = "/getJadwal/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getJadwalById(@PathVariable("id") int id, Model model) {
		model.addAttribute("jadwal", this.jadwalService.getJadwal(id));
		return "jadwalById";
	}
	
	@RequestMapping(value = "/addJadwal", method = RequestMethod.POST, headers = "Accept=application/json")
	public String addJadwal(@ModelAttribute("jadwal") Jadwal jadwal) {
		Ruangan ruangan = ruanganService.getRuangan(jadwal.getId_ruangan());
		if(jadwal.getId() == 0) {
			jadwalService.addJadwal(jadwal);		
		}
		else {
			jadwalService.updateJadwal(jadwal);
		}
		return "redirect:/getAllJadwal";
	}
	
	@RequestMapping(value = "/updateJadwal/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String updateJadwal(@PathVariable("id") int id, Model model) {
		List listOfRuangan = ruanganService.getAllRuangan();
		model.addAttribute("listOfRuangan", listOfRuangan);
		model.addAttribute("jadwal", this.jadwalService.getJadwal(id));
		model.addAttribute("listOfJadwal", this.jadwalService.getAllJadwal());
		return "detailJadwal";
	}
	
	@RequestMapping(value = "/deleteJadwal/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String deleteJadwal(@PathVariable("id") int id) {
		jadwalService.deleteJadwal(id);
		return "redirect:/getAllJadwal";
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public String downloadData(Model model) throws IOException {
		jadwalService.downloadData();
		String path = System.getProperty("user.dir") + "\\ExcelFile";
		model.addAttribute("msg", path);
		return "statusDownload";
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String uploadData(@RequestParam("files") MultipartFile[] files) throws IOException {
		String path = System.getProperty("user.dir") + "\\ExcelFile";
		StringBuilder fileNames = new StringBuilder();
		for (MultipartFile file : files) {
			Path fileNameAndPath = Paths.get(path, "Test.xlsx");
			fileNames.append(file.getOriginalFilename()+" ");
			Files.write(fileNameAndPath, file.getBytes());			
		}
		jadwalService.uploadData();
		return "redirect:/getAllJadwal";
	}
}
