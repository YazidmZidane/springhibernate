package com.yazid.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yazid.model.Jadwal;

@Repository
public class JadwalDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<Jadwal> getAllJadwal() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Jadwal> jadwalList = session.createQuery("from Jadwal Order By id_jadwal").list();
		return jadwalList;
	}
	
	public Jadwal getJadwal(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Jadwal jadwal = (Jadwal) session.load(Jadwal.class, new Integer(id));
		return jadwal;
	}
	
	public Jadwal addJadwal(Jadwal jadwal) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(jadwal);
		return jadwal;
	}
	
	public void updateJadwal(Jadwal jadwal) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(jadwal);
	}
	
	public void deleteJadwal(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Jadwal j = (Jadwal) session.load(Jadwal.class, new Integer(id));
		if(j != null) {
			session.delete(j);
		}
	}
	
	public void downloadData() throws IOException {
		Session session = this.sessionFactory.getCurrentSession();
		TypedQuery query=session.createQuery("from Jadwal Order By id_jadwal");    
	    List<Jadwal> list=query.getResultList();

		XSSFWorkbook workbook = new XSSFWorkbook(); 
		XSSFSheet spreadsheet = workbook.createSheet("Jadwal_DB");

		XSSFRow row = spreadsheet.createRow(1);
		XSSFCell cell;
		cell = row.createCell(1);
		cell.setCellValue("Id");
		cell = row.createCell(2);
		cell.setCellValue("No Ruangan");
		cell = row.createCell(3);
		cell.setCellValue("Nama Pengguna");
		cell = row.createCell(4);
		cell.setCellValue("Waktu Mulai");
		cell = row.createCell(5);
		cell.setCellValue("Waktu Selesai");
		cell = row.createCell(6);
		cell.setCellValue("Deskripsi");
		int i = 2;
		
		Iterator<Jadwal> itr = list.iterator();
		while(itr.hasNext()) {
			Jadwal jdwl = itr.next(); 
			row = spreadsheet.createRow(i);
			cell = row.createCell(1);
			cell.setCellValue(jdwl.getId());
			cell = row.createCell(2);
			cell.setCellValue(jdwl.getRuangan().getNo_ruangan());
			cell = row.createCell(3);
			cell.setCellValue(jdwl.getNama_pengguna());
			cell = row.createCell(4);
			cell.setCellValue(jdwl.getWaktu_mulai());
			cell = row.createCell(5);
			cell.setCellValue(jdwl.getWaktu_selesai());
			cell = row.createCell(6);
			cell.setCellValue(jdwl.getDetail_kegiatan());
			i++;
		}
		Date dt = new Date();
		String path = System.getProperty("user.dir") + "\\ExcelFile";
		FileOutputStream fos = new FileOutputStream(new File(path+"\\Test"+dt.getDate()+dt.getMonth()+dt.getYear()+dt.getTime()+".xlsx"));
		workbook.write(fos);
		fos.close();
	}
	
	public void uploadData() throws IOException {
		Session session = this.sessionFactory.getCurrentSession();
	    
	    String path = System.getProperty("user.dir") + "\\ExcelFile";
	    FileInputStream fis = new FileInputStream(new File(path+"\\Test.xlsx"));
	    Workbook workbook = new XSSFWorkbook(fis);
		XSSFSheet spreadsheet = (XSSFSheet) workbook.getSheetAt(0);
		XSSFRow row;
		XSSFCell cell;
		Iterator <Row>  rowIterator = spreadsheet.iterator();
		int i = 2;
		while(rowIterator.hasNext()) {
			Jadwal jdwl = new Jadwal();
			row = spreadsheet.getRow(i);
			if (row == null) {
				break;
			}
			cell = row.getCell(1);
			jdwl.setId((int) cell.getNumericCellValue());
			cell = row.getCell(2);
			Query query = session.createQuery("SELECT id_ruangan FROM Ruangan where no_ruangan = :ruangan");
			query.setParameter("ruangan", cell.getStringCellValue());
			jdwl.setId_ruangan((int) query.getSingleResult());
			cell = row.getCell(3);
			jdwl.setNama_pengguna(cell.getStringCellValue());
			cell = row.getCell(4);
			jdwl.setWaktu_mulai(String.valueOf(cell.getNumericCellValue()));
			cell = row.getCell(5);
			jdwl.setWaktu_selesai(String.valueOf(cell.getNumericCellValue()));
			cell = row.getCell(6);
			jdwl.setDetail_kegiatan(cell.getStringCellValue());
			session.save(jdwl);
			i++;
		}
		fis.close();
	}
}
