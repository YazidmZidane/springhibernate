package com.yazid.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yazid.model.Ruangan;

@Repository
public class RuanganDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<Ruangan> getAllRuangan(){
		Session session = this.sessionFactory.getCurrentSession();
		List<Ruangan> ruanganList = session.createQuery("from Ruangan").list();
		return ruanganList;
	}
	
	public Ruangan getRuangan(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Ruangan ruangan = (Ruangan) session.load(Ruangan.class, new Integer(id));
		return ruangan;
	}
	
	public Ruangan addRuangan(Ruangan ruangan) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(ruangan);
		return ruangan;
	}
	
	public void updateRuangan(Ruangan ruangan) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(ruangan);
	}
	
	public void deleteJadwal(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Ruangan j = (Ruangan) session.load(Ruangan.class, new Integer(id));
		if(j != null) {
			session.delete(j);
		}
	}
}
