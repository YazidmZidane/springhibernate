package com.yazid.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "jadwal")
@Proxy(lazy=false)
public class Jadwal {
	
	@Id
	@Column(name = "id_jadwal")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name = "id_ruangan")
	int id_ruangan;
	
	@Column(name = "nama_pengguna")
	String nama_pengguna;
	
	@Column(name = "waktu_mulai")
	String waktu_mulai;
	
	@Column(name = "waktu_selesai")
	String waktu_selesai;
	
	@Column(name = "detail_kegiatan")
	String detail_kegiatan;
	
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name="id_ruangan", updatable=false, insertable=false)
	private Ruangan ruangan;
	
	public Jadwal() {
		super();
	}
	
	public Jadwal(int id, int id_ruangan, String nama_pengguna, String waktu_mulai, String waktu_selesai, String nama_kegiatan, String detail_kegiatan, Ruangan ruangan) {
		super();
		this.id = id;
		this.id_ruangan = id_ruangan;
		this.nama_pengguna = nama_pengguna;
		this.waktu_mulai = waktu_mulai;
		this.waktu_selesai = waktu_selesai;
		this.detail_kegiatan = detail_kegiatan;
		this.ruangan = ruangan;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	

	public int getId_ruangan() {
		return id_ruangan;
	}

	public void setId_ruangan(int id_ruangan) {
		this.id_ruangan = id_ruangan;
	}

	public String getNama_pengguna() {
		return nama_pengguna;
	}

	public void setNama_pengguna(String nama_pengguna) {
		this.nama_pengguna = nama_pengguna;
	}

	public String getWaktu_mulai() {
		return waktu_mulai;
	}

	public void setWaktu_mulai(String waktu_mulai) {
		this.waktu_mulai = waktu_mulai;
	}

	public String getWaktu_selesai() {
		return waktu_selesai;
	}

	public void setWaktu_selesai(String waktu_selesai) {
		this.waktu_selesai = waktu_selesai;
	}

	public String getDetail_kegiatan() {
		return detail_kegiatan;
	}

	public void setDetail_kegiatan(String detail_kegiatan) {
		this.detail_kegiatan = detail_kegiatan;
	}
	
	public Ruangan getRuangan() {
		return ruangan;
	}
	
	public void setRuangan(Ruangan ruangan) {
		this.ruangan = ruangan;
	}
	
}