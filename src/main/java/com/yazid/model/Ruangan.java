package com.yazid.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ruangan")
public class Ruangan {
	
	@Id
	@Column(name = "id_ruangan", nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id_ruangan;	
	
	@Column(name = "no_ruangan")
	String no_ruangan;
	
	@Column(name = "jenis_ruangan")
	String jenis_ruangan;
		
	
	public Ruangan() {
		super();
	}
	
	public Ruangan(int id_ruangan, String no_ruangan, String jenis_ruangan) {
		super();
		this.id_ruangan = id_ruangan;
		this.no_ruangan = no_ruangan;
		this.jenis_ruangan = jenis_ruangan;
	}

	public int getId_ruangan() {
		return id_ruangan;
	}

	public void setId_ruangan(int id_ruangan) {
		this.id_ruangan = id_ruangan;
	}

	public String getNo_ruangan() {
		return no_ruangan;
	}

	public void setNo_ruangan(String no_ruangan) {
		this.no_ruangan = no_ruangan;
	}

	public String getJenis_ruangan() {
		return jenis_ruangan;
	}

	public void setJenis_ruangan(String jenis_ruangan) {
		this.jenis_ruangan = jenis_ruangan;
	}
	
}
