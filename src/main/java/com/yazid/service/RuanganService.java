package com.yazid.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yazid.dao.RuanganDao;
import com.yazid.model.Ruangan;

@Service("ruanganService")
public class RuanganService {
	
	@Autowired
	RuanganDao ruanganDao;
	
	@Transactional
	public List<Ruangan> getAllRuangan() {
		return ruanganDao.getAllRuangan();
	}
	
	@Transactional
	public Ruangan getRuangan(int id) {
		return ruanganDao.getRuangan(id);
	}
	
	@Transactional
	public void addRuangan(Ruangan ruangan) {
		ruanganDao.addRuangan(ruangan);
	}
	
	@Transactional
	public void updateRuangan(Ruangan ruangan) {
		ruanganDao.updateRuangan(ruangan);
	}
	
	public void deleteRuangan(int id) {
		ruanganDao.deleteJadwal(id);
	}
}
