package com.yazid.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yazid.dao.JadwalDao;
import com.yazid.model.Jadwal;

@Service("jadwalService")
public class JadwalService {
	
	@Autowired
	JadwalDao jadwalDao;
	
	@Transactional
	public List<Jadwal> getAllJadwal() {
		return jadwalDao.getAllJadwal();
	}
	
	@Transactional
	public Jadwal getJadwal(int id) {
		return jadwalDao.getJadwal(id);
	}
	
	@Transactional
	public void addJadwal(Jadwal jadwal) {
		jadwalDao.addJadwal(jadwal);
	}
	
	@Transactional
	public void updateJadwal(Jadwal jadwal) {
		jadwalDao.updateJadwal(jadwal);
	}
	
	@Transactional
	public void deleteJadwal(int id) {
		jadwalDao.deleteJadwal(id);
	}
	
	@Transactional
	public void downloadData() throws IOException {
		jadwalDao.downloadData();
	}
	
	@Transactional
	public void uploadData() throws IOException {
		jadwalDao.uploadData();
	}
}