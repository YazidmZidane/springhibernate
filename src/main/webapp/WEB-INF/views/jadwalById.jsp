<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Detail Jadwal</title>
</head>
<body>
<div class="container">
  <table>
		<tr>
			<td>No Ruangan</td>
			<td>:</td>
			<td>${jadwal.ruangan.no_ruangan } --- ${jadwal.ruangan.jenis_ruangan}</td>
		</tr>
		<tr>
			<td>Nama Pengguna</td>
			<td>:</td>
			<td>${jadwal.nama_pengguna }</td>
		</tr>
		<tr>
			<td>Waktu Mulai</td>
			<td>:</td>
			<td>${jadwal.waktu_mulai }</td>
		</tr>
		<tr>
			<td>Waktu Selesai</td>
			<td>:</td>
			<td>${jadwal.waktu_selesai }</td>
		</tr>
		<tr>
			<td>Detail Kegiatan</td>
			<td>:</td>
			<td>${jadwal.detail_kegiatan }</td>
		</tr>
	</table>
	<a href="<c:url value='/getAllJadwal' />">Kembali</a>   
</div>	
</body>
</html>