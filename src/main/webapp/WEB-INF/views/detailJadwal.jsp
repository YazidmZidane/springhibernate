<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<html>
<head>
<title>Jadwal</title>
<style>
.blue-button {
	background: #25A6E1;
	filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#25A6E1',
		endColorstr='#188BC0', GradientType=0);
	padding: 3px 5px;
	color: #fff;
	font-family: 'Helvetica Neue', sans-serif;
	font-size: 12px;
	border-radius: 2px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 4px;
	border: 1px solid #1A87B9
}

table {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	width: 50%;
	font-stlye: normal;
}

th {
	background: SteelBlue;
	color: white;
	text-align: center;
}

td, th {
	border: 1px solid gray;
	font-stlye: normal;	
	padding: 5px 10px;
}

.row {
	margin-top: 40px;
	padding: 0 10px;
}

.clickable {
	cursor: pointer;
}

.panel-heading div {
	margin-top: -18px;
	font-size: 15px;
}

.panel-heading div span {
	margin-left: 5px;
}

.panel-body {
	display: none;
}
</style>
</head>
<body>
<br>
	<div class="container">		
		<div class="col-md-12">
			<form action="/SpringMVCHibernateCRUD/upload" method="post" enctype="multipart/form-data">
				<input type="file" name="files" multiple class="form-control" required><br><input
					type="submit" value="Upload Files" class="btn btn-info btn-md"></input>
			</form>
			<button type="button" class="btn btn-info btn-md" onclick="window.location.href='<c:url value='/download' />'">Download Data</button>		
			<button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal">Tambah Data</button>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary">					
					<div class="panel-heading">
						<h3 class="panel-title">List Jadwal</h3>
					</div>
					<div class="panel-body">
						<input type="text" class="form-control" id="dev-table-filter"
							data-action="filter" data-filters="#dev-table"
							placeholder="Filter Developers" />
					</div>					
					<c:if test="${!empty listOfJadwal}">
						<table class="table table-hover" id="dev-table">
							<thead>
								<tr>
									<th width="5%">Id</th>
									<th width="20%">No Ruangan</th>
									<th width="20%">Nama Pengguna</th>
									<th width="15%">Waktu Mulai</th>
									<th width="15%">Waktu Selesai</th>
									<th width="5%">Ubah</th>
									<th width="5%">Hapus</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listOfJadwal}" var="jadwal">
									<tr
										onclick="window.location='<c:url value='/getJadwal/${jadwal.id}' />'">
										<td align="center">${jadwal.id}</td>
										<td>${jadwal.ruangan.no_ruangan} --- ${jadwal.ruangan.jenis_ruangan}</td>
										<td>${jadwal.nama_pengguna}</td>
										<td>${jadwal.waktu_mulai}</td>
										<td>${jadwal.waktu_selesai}</td>
										<td align="center"><a
											href="<c:url value='/updateJadwal/${jadwal.id}' />"><i class="fa fa-pencil-square-o" style="font-size:24px"></i></a></td>
										<td align="center"><a
											href="<c:url value='/deleteJadwal/${jadwal.id}' />"><i class="fa fa-trash-o" style="color:red;font-size:24px"></i></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>
				</div>
			</div>
		</div>				
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Tambah Data</h4>
					</div>
					<div class="modal-body">
						<form:form method="post" modelAttribute="jadwal"
							action="/SpringMVCHibernateCRUD/addJadwal">
							<table class="table table-hover" id="dev-table">
								<tr>
									<th colspan="2">Data</th>
								</tr>
								<tr>
									<form:hidden path="id" />
									<td>No Ruangan</td>
									<td><form:select path="id_ruangan" class="form-control">
										<c:forEach items="${listOfRuangan}" var="ruangan">
											<form:option value="${ruangan.id_ruangan}">${ruangan.no_ruangan } --- ${ruangan.jenis_ruangan}</form:option>
										</c:forEach>
									</form:select></td>
								</tr>
								<tr>
									<td>Nama Pengguna</td>
									<td><form:input path="nama_pengguna" size="30"
											maxlength="30" class="form-control"></form:input></td>
								</tr>
								<tr>
									<td>Waktu Mulai</td>
									<td><form:input type="time" path="waktu_mulai" size="30"
											maxlength="30" class="form-control"></form:input></td>
								</tr>
								<tr>
									<td>Waktu Selesai</td>
									<td><form:input type="time" path="waktu_selesai" size="30"
											maxlength="30" class="form-control"></form:input></td>
								</tr>
								<tr>
									<td>Detail Kegiatan</td>
									<td><form:textarea path="detail_kegiatan" class="form-control" style="resize: none;"></form:textarea></td>
								</tr>
								<tr>
									<td colspan="2"><input type="submit" class="blue-button"
										value="Simpan" /></td>
								</tr>
							</table>
						</form:form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>
</html>
